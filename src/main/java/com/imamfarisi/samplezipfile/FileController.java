package com.imamfarisi.samplezipfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("files")
public class FileController {
	
	@Autowired
	private FileService fileService;

	@GetMapping("zip/{id}")
	public ResponseEntity<?> zipFiles(@PathVariable("id") Long id) throws Exception {

		byte[] result = fileService.zipFiles();

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=attachment.zip")
				.body(result);

	}
}
