package com.imamfarisi.samplezipfile;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {

	public static byte[] zipFiles(List<byte[]> files, List<String> extensions) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(baos);

		int totalFiles = files.size();
		for (int i = 0; i < totalFiles; i++) {
			byte[] file = files.get(i);
			String ext = extensions.get(i);
			ZipEntry entry = new ZipEntry("attachment" + i + "." + ext);
			entry.setSize(file.length);
			zos.putNextEntry(entry);
			zos.write(file);
		}

		zos.closeEntry();
		zos.close();
		
		return baos.toByteArray();
	}

}
